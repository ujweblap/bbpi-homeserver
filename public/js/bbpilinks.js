(function(){
    var app = angular.module('bbpi.links',['ui.bootstrap']);
    app.controller('linkCtrl', ['$scope', '$log', function($scope, $log){
        $scope.links = [
            {title:'BenceBananaPi - homeServer',url:'http://bbpi:8080/'},
            {title:'BenceBananaPi2 - homeServer',url:'http://bbpi:8080/'},
            {title:'BenceBananaPi3 - homeServer',url:'http://bbpi:8080/'},
            {title:'BenceBananaPi4 - homeServer',url:'http://bbpi:8080/'},
            {title:'BenceBananaPi5 - homeServer',url:'http://bbpi:8080/'}
        ];
        $scope.totalItems = 64;
        $scope.currentPage = 4;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            $log.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.maxSize = 5;
        $scope.bigTotalItems = 175;
        $scope.bigCurrentPage = 1;
    }]);
})();
