(function(){
    var bbpi = angular.module('bbpi',['ui.bootstrap','ui.router','bbpi.links'])
    .value('SiteIp','bbpi')
    .value('SitePort',8080)
    .service('menuService', function(){
        this.menuList = function(){
            return [
                {name:'Home',state:'/'},
                {name:'Favorites',state:'/favs'},
                {name:'piDrive',state:'/pidrive'},
                {name:'chatIO',state:'/chat'}
            ];
        };
        //return menuList;
    });
    bbpi.factory('socket', function ($rootScope) {
        var socket = io.connect();
        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            }
        };
    });
    bbpi.controller('startCtrl',['$scope',function($scope){

    }]);
    bbpi.controller('apiCtrl',['$scope','$http',function($scope,$http){
        $http({method:'GET',url:'/api',contentType:'JSON'}).success(function(res){
            $scope.status = res;
        });
    }]);
    bbpi.controller('initCtrl', ['$scope', 'socket', function($scope, socket){
        $scope.Site = {};
        $scope.Site.alerts = [];
        $scope.addAlert = function(msg, alertType) {
            if(alertType==undefined){
                alertType='danger';
            }
            $scope.Site.alerts.push({type:alertType, msg: msg});
        };
        $scope.closeAlert = function(index) {
            $scope.Site.alerts.splice(index, 1);
        };
        socket.emit('init',{status:'initCtrl'});
        socket.on('bbpi:init', function(initData){
            $scope.addAlert('Hello '+initData.user,'success');
        });
    }]);
    bbpi.controller('menuCtrl',['$scope','menuService', function($scope, menuService){
        $scope.Site.menu = menuService.menuList();
    }]);
    bbpi.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        //
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("/");
        //
        // Now set up the states
        $stateProvider
        .state('/', {
            url: "/home",
            templateUrl: "partials/home.html"
        })
        .state('/favs', {
            url: "/favs",
            templateUrl: "partials/links.html",
            controller: function($scope) {
                $scope.items = ["A", "List", "Of", "Items"];
            }
        })
        .state('state2', {
            url: "/state2",
            templateUrl: "partials/state2.html"
        })
        .state('state2.list', {
            url: "/list",
            templateUrl: "partials/state2.list.html",
            controller: function($scope) {
                $scope.things = ["A", "Set", "Of", "Things"];
            }
        });
    });
})();
