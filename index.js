var express = require('express');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var socket = require('socket.io');
var app = express();
var http = require('http').Server(app);
var io = socket(http);
var port = 8080;
var os = require('os');
var public_dir = '/public';
var fdir = require('./libs/fdir').setDir(__dirname+public_dir);
require('colors');
var users = require('./libs/users');

//app.use("/public", express.static(path.join(__dirname, 'public')));
app.use(express.static(fdir.get()));

app.use(session({
    store: new RedisStore({
        host: '127.0.0.1',
        port: 6379
    }),
    secret: 'super secret !word',
    resave: false,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    if(req.url=='/login'){
        if(Users.checkReq(req)){
            req.url = '/';
        }
        next();
    }

    if(!req.session || (req.session && !req.session.loggedIn)){
        res.sendFile(fdir.get('login.html'));
        //req.url = '/login';
    } else {
        req.url = '/#'+req.url;
    }
    /*
  if (!req.session) {
    return next(new Error('oh no')); // handle error
  }*/
  next();
});

/*
app.get('/:url', function(req, res){
    app.get('/');
});
*/

app.get('/', function(req, res){
    console.log('Client connected to '+'/'.cyan);
    res.sendFile(fdir.get('index.html'));
});

app.get('/status', function(req, res){
    //console.log(req.connection);
    res.sendFile(fdir.get('status.html'));
});

app.get('/login', function(req, res){
    //console.log(req.connection);
    req.session.loggedIn = true;
    req.session.userId = '88';
    res.send('logged in! ;)');
});

app.get('/api', function(req, res){
    res.send({
        status: 200,
        hostname: os.hostname()
    });
});

app.post('/counter/:cont',function(req,res){
    console.log(req.params.cont);
    counter = req.param.cont;
    counter ++;
    res.send(counter);
});

app.get('/hello/:name', function(req, res){
    console.dir(req);
    console.log("hello bbpi. name: "+req.params.name);
    res.send('hello '+req.params.name);
});

/* socketIO events */

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('init', function(dt){
        console.log(dt)
        socket.emit('bbpi:init', {user:'bbpi'});
    });
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

http.listen(port, function(){
    console.log('Server Listen ON '+(port+'').green);
});
console.log('Server starting... '.inverse);
console.log('Host name: '+os.hostname().yellow);
