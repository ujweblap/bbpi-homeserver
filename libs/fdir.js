module.exports = (function(){
    this.dirName = __dirname;
    this.setDir = function(dir){
        this.dirName = dir;
        return this;
    };
    this.get = function(file){
        if(typeof file==='undefined'){
            return this.dirName;
        }
        return this.dirName+'/'+file;
    };
    return this;
})();